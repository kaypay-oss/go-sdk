# KayPay SDK for Go

## Demo

https://go.dev/play/p/Fs1R8IY0kf1

```go
package main

import (
	"fmt"

	"gitlab.com/kaypay-oss/go-sdk/kaypay"
)

func main() {
	signer := kaypay.NewSigner([]byte("s3cret"))
	fmt.Println(string(signer.SignData([]byte("{\"foo\":\"bar\"}"))))
}
```

Output

```
v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=
```
