package kaypay

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

var data = []byte("{\"foo\":\"bar\"}")
var secretKey = []byte("s3cret")
var signer = NewSigner(secretKey)

func TestGetVersion(t *testing.T) {
	version := signer.GetVersion()
	assert.Equal(t, "v1", version)
}

func TestMakeHeaders(t *testing.T) {
	headers := signer.MakeHeaders(data)
	expected := map[string]string{
		"X-KayPay-Signature": "v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=",
	}
	assert.Equal(t, expected, headers)
}

func TestSignData(t *testing.T) {
	signature := signer.SignData(data)
	assert.Equal(t, "v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=", string(signature))
}

func TestSignRequest(t *testing.T) {
	req, _ := http.NewRequest(http.MethodPost, "https://postman-echo.com/post", bytes.NewBuffer(data))
	signErr := signer.SignRequest(req)
	assert.Nil(t, signErr)

	resp, _ := http.DefaultClient.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	var echo postmanEcho
	_ = json.Unmarshal(body, &echo)

	echoJson, _ := json.Marshal(echo.Json)
	assert.Equal(t, string(data), string(echoJson))

	assert.Equal(t, "v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=", echo.Headers["x-kaypay-signature"])
}

func TestSignRequestNonPost(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	err := signer.SignRequest(req)
	assert.NotNil(t, err)
}

func TestVerifyRequest(t *testing.T) {
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(data))
	req.Header.Set("X-KayPay-Signature", "v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=")

	verified, _ := signer.VerifyRequest(req)
	assert.True(t, verified)
}

func TestVerifyRequestNonPost(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	_, err := signer.VerifyRequest(req)
	assert.NotNil(t, err)
}

func TestVerifySignature(t *testing.T) {
	verified := signer.VerifySignature(data, "v1=/fcKOYJllgEc0g5zGa5tolBHTXGq7L3j5LmbalcGV1c=")
	assert.True(t, verified)
}

func TestVerifySignatureFalse(t *testing.T) {
	verified := signer.VerifySignature(data, "foo")
	assert.False(t, verified)
}

type postmanEcho struct {
	Headers map[string]string `json:"headers"`
	Json    interface{}       `json:"json"`
}
