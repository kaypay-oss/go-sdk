package kaypay

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const SignatureHeader = "X-KayPay-Signature"

const signatureVersion1 = "v1"

type Signer struct {
	secretKey []byte
	version   string
}

func NewSigner(secretKey []byte) *Signer {
	return NewSignerV1(secretKey)
}

func NewSignerV1(secretKey []byte) *Signer {
	return &Signer{
		secretKey: secretKey,
		version:   signatureVersion1,
	}
}

func (s Signer) GetVersion() string {
	return s.version
}

func (s Signer) MakeHeaders(data []byte) map[string]string {
	return map[string]string{
		SignatureHeader: string(s.SignData(data)),
	}
}

func (s Signer) SignData(data []byte) []byte {
	versionPrefix := []byte(fmt.Sprintf("%s=", s.version))

	// v1 algorithm
	hash := hmac.New(sha256.New, s.secretKey)
	hash.Write(data)
	hashed := hash.Sum(nil)

	b64Std := base64.StdEncoding
	signature := make([]byte, len(versionPrefix)+b64Std.EncodedLen(len(hashed)))
	copy(signature, versionPrefix)

	buf := signature[len(versionPrefix):]
	b64Std.Encode(buf, hashed)

	return signature
}

func (s Signer) SignRequest(request *http.Request) error {
	if request.Method != http.MethodPost {
		return errors.New("only POST request can be signed")
	}

	body, _ := readBodyWithoutSideEffect(request)
	headers := s.MakeHeaders(body)
	for key, value := range headers {
		request.Header.Set(key, value)
	}

	return nil
}

func (s Signer) VerifyRequest(request *http.Request) (bool, error) {
	if request.Method != http.MethodPost {
		return false, errors.New("only POST request can be signed")
	}

	body, _ := readBodyWithoutSideEffect(request)
	signature := request.Header.Get(SignatureHeader)
	return s.VerifySignature(body, signature), nil
}

func (s Signer) VerifySignature(data []byte, signature string) bool {
	expected := string(s.SignData(data))
	singles := strings.Split(signature, ",")
	for _, single := range singles {
		if single == expected {
			return true
		}
	}

	return false
}

func readBodyWithoutSideEffect(request *http.Request) ([]byte, error) {
	body, err := ioutil.ReadAll(request.Body)
	request.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	return body, err
}
